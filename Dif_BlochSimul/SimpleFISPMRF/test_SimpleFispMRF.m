% create some dummy sequence of FA's and TRs
N = 101;
FA = [180 (sin(linspace(0,pi,N-1))*.7+sin(linspace(0,2*pi,N-1))*.3+sin(linspace(0,3*pi,N-1))*.15+.1)*60]/180*pi;
TR = 15;
TE = 3;

% Create interesting T1/T2 range
T1 = [1 5:5:100 110:10:200 250:50:1000 1100:100:5000];
T2 = [1:10 12:2:50 55:5:200 210:10:300 320:20:500 550:50:1000];
% restrict T1/T2 values to physically possible domain (T1>T2)
[T1,T2] = meshgrid(T1,T2);
I = find(T1>T2);
T1 = T1(I);
T2 = T2(I);

% Calculate Response


% dict = SimpleFispMRF(FA,TR,T1,T2,TE);

dict = SimpleFispMRF(single(FA), single(TR), ...
    single(T1),single(T2),single(TE));


% In Fingerprinting we would additionally normalize each signal. Enable if
% needed
if 1
dict = dict./sqrt(sum(abs(dict).^2,1));
end
