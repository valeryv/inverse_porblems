function out = SimpleFispMRF(FA,TR,T1,T2,TE)
%%SimpleFispMRF Runs an EPG Simulation to Simulate Signal for FispMRF
%Input Paramter:
%  FA   Complex Flip Angle Array 
%        +Magnitude: Flip Angle    (rad)
%        +Phase: RF Pulse phase    (rad)
%  TR   Repetition Time Array      (ms)
%  T1   T1 Value Array             (ms)
%  T2   T2 Value Array             
%        +Magnitude: T2 Decay      (ms)
%        +Phase: Off-Resonance     (kHz)
%  (TE) Optional: Echo Time Array  (ms)
%
% This code uses the EPG formalism to calculate the signal response of a
% spoiled SSFP sequence with variable flip angle, TR and TE for a given set
% of T1/T2 values. Off-resonance can be included by choosing T2 complex
% (the phase argument is then the NEGATIVE off-resonance in units of kHz)
%
% The basis of the implementation is the EPG review paper by M. Weigel
% > Weigel, M. Extended phase graphs: Dephasing, RF pulses, and echoes - pure and simple. J. Magn. Reson. Imaging 41, 266�295 (2015).
% And the source code provided by Brian Hargreaves, Stanford: http://www.stanford.edu/~bah/software/epg/ 
%
% (2019-05-05) Christian Guenthner, guenthner@biomed.ee.ethz.ch

if nargin<5 || isempty(TE); TE = TR/2; end

% reshape inputs
FA = reshape(FA,[],1);
TR = reshape(TR,[],1);
TE = reshape(TE,[],1);
T1 = reshape(T1,1,[]);
T2 = reshape(T2,1,[]);

% obtain sequence length
N = length(FA);
Nepg = ceil(length(FA)/2);
Niso = length(T1);

% print stats
fprintf('Sequence Length: %i\n',N);
fprintf('Parameter Space: %i\n',Niso);

% replicate optional arrays
if length(TR) == 1; TR = repmat(TR,N,1); end
if length(TE) == 1; TE = repmat(TE,N,1); end

% Test Array Lengths
assert(N == length(TR),'Size of TR and FA must be equal.');
assert(N == length(TE),'Size of TR, FA & TE must be equal.');
assert(Niso == length(T2),'Size of T1 & T2 must be equal.');

% Initialize Magnetization Distribution
mag = zeros(3,Niso,Nepg, 'like', T1);
mag(3,:,1) = 1;
sz = size(mag);

Niso
Nepg
N

size(TE)
size(TR)
size(FA)
size(T1)
size(T2)

% Initialize Output
out = zeros(N,Niso, 'like', T1);

% Iterate over Sequence
progressex('Calculation System Response',1);
for n=1:N
%     progressex(n/N*100);
    
    % RF pulse
    rm = RMat(angle(FA(n)),abs(FA(n)));
    
    fprintf('iii %e %e %e %e \n', abs(FA(n)), angle(FA(n)), TR(n), TE(n));
    rm
    
        
    
    mag = reshape(RMat(angle(FA(n)),abs(FA(n))) * mag(:,:),sz);
    
    fprintf('mv mean %e\n', mean(mag(:)));

    % relax to readout
    mag = relax(TE(n),T1,T2,mag);    
    
    fprintf('mr mean %e\n', mean(mag(:)));

    
    % readout
    out(n,:) = mag(1,:,1);
    
    % spoil transverse states
    mag = spoil(mag);
    
    fprintf('ms mean %e\n', mean(mag(:)));
    

    
    % and relax
    mag = relax(TR(n)-TE(n),T1,T2,mag);    
    fprintf('mr2 mean %e\n', mean(mag(:)));

    
%         mag(:, 1)
%     pause;
end
progressex(100);
progressex('done.');

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% EPG HELPER FUNCTIONS AFTER THIS POINT                         %%%%%% 

function mag=relax(t,T1,T2,mag)
%RELAX Relaxes magnetization mag by time t
% t    Time Interval to Relax (ms)
% T1   T1 Value Array         (ms)
% T2   T2 Value Array (may be complex for off-resonance) (ms)
% mag  Magnetization Vector (3xNT1xNepg);
    mag = [exp(-t./T2); exp(-t./T2); exp(-t./T1)] .* mag; %< decay
    mag(3,:,1) = mag(3,:,1) + (1-exp(-t./T1)); %< recovery
end

function [ res ] = RMat( phi, alpha )
%RMat Makes Rotation Matrix for RF Pulse
% phi    RF pulse phase       (rad)
% alpha  RF flip angle        (rad)
% Source: Weigel, M. Extended phase graphs: Dephasing, RF pulses, and echoes - pure and simple. J. Magn. Reson. Imaging 41, 266�295 (2015).

    res = [cos(alpha/2).^2 exp(2*i*phi).*sin(alpha/2).^2 -i*exp(i*phi).*sin(alpha);...
        exp(-2*i*phi).*sin(alpha/2).^2 cos(alpha/2).^2 i*exp(-i*phi).*sin(alpha);...
        -i/2*exp(-i*phi).*sin(alpha) i/2*exp(i*phi).*sin(alpha) cos(alpha)];
end

function mag=spoil( mag)
%SPOIL Spoils Transverse Magneization via Configuration State Shift
% mag   Magnetization Vector (3xNT1xNepg)

    mag(1,:,2:end) = mag(1,:,1:end-1); 
    mag(2,:,1:end-1) = mag(2,:,2:end); 
    mag(2,:,end) = 0;
    mag(1,:,1) = conj(mag(2,:,1));   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% HELPER FOR NICE PROGRESS OUTPUT...                            %%%%%% 

function progressex(c,init)
% This function creates a text progress bar. It should be called with a 
% STRING argument to initialize and terminate. Otherwise the number correspoding 
% to progress in % should be supplied.
% INPUTS:   C   Either: Text string to initialize or terminate 
%                       Percentage number to show progress 
% OUTPUTS:  N/A
% Example:  Please refer to demo_textprogressbar.m

% Author: Paul Proteus (e-mail: proteus.paul (at) yahoo (dot) com)
% Version: 1.0
% Changes tracker:  29.06.2010  - First version

% Inspired by: http://blogs.mathworks.com/loren/2007/08/01/monitoring-progress-of-a-calculation/

%% Initialization
persistent strCR;           %   Carriage return pesistent variable

if nargin<2 || isempty(init); init=false; end

% Vizualization parameters
strPercentageLength = 10;   %   Length of percentage string (must be >5)
strDotsMaximum      = 10;   %   The total number of dots in a progress bar

%% Main 
if ~init && isempty(strCR) && ~ischar(c)
    % Progress bar must be initialized with a string
    error('The text progress must be initialized with a string');
elseif (init || isempty(strCR)) && ischar(c)
    % Progress bar - initialization
    fprintf('%s',c);
    strCR = -1;
elseif ~init && ~isempty(strCR) && ischar(c)
    % Progress bar  - termination
    strCR = [];  
    fprintf([c '\n']);
elseif isnumeric(c)
    % Progress bar - normal progress
    c = floor(c);
    percentageOut = [num2str(c) '%%'];
    percentageOut = [percentageOut repmat(' ',1,strPercentageLength-length(percentageOut)-1)];
    nDots = floor(c/100*strDotsMaximum);
    dotOut = ['[' repmat('.',1,nDots) repmat(' ',1,strDotsMaximum-nDots) ']'];
    strOut = [percentageOut dotOut];
    
    if init
        if init<1
            strOut = [strOut '(ETA: ' num2str(init*60) ' sec.)'];
        else
            strOut = [strOut '(ETA: ' num2str(init,1) ' min.)'];
        end
    end
    
    % Print it on the screen
    if strCR == -1,
        % Don't do carriage return during first run
        fprintf(strOut);
    else
        % Do it during all the other runs
        fprintf([strCR strOut]);
    end
    
    % Update carriage return
    strCR = repmat('\b',1,length(strOut)-1);
    
else
    % Any other unexpected input
    error('Unsupported argument type');
end
end