function [rec, itinfo] = deconv_3d_sparse(b, psf, w, lambda, lambda_TV, base, MAX_ITER, mtolX, debug)
% this function solves the following problem:
% min_x  0.5*|w.*(conv(x, psf) - b)|^2 + lambda |x-base|_1 + lambda_TV|Dx|_{2,1}
% via ADMM 

% b - measurements (zero-filled)
% psf - psf array
% w - mask\weighting array
% lambda - sparsity regularization
% lambda_TV - isotropic TV regularization
% base - low res image (can be zeros)
% MAX_ITER - max ADMM iterations
% mtolX - desired termination tolerance in X update
% debug - debug info level

X_upd_type = 'GD';

% normalize operators
k1 = sum(psf(:));
k2 = max(abs(w(:)));
psf = psf / k1;
b = b / k1;
w = w / k2;
b = b / k2;

w2 = abs(w).^2;
if isempty(base)
    base = b * 0;
end
% prepare for fft conv
psfp = padarray(psf, size(b) - size(psf), 'post');
shftd = floor(size(psf)/2);
psfp = circshift(psfp, -shftd(1), 1);
psfp = circshift(psfp, -shftd(2), 2);
psfp = circshift(psfp, -shftd(3), 3);
k_psfp = fftn(psfp);

kb = fftn(b);
x = (conj(k_psfp) .* kb) ./ (abs(k_psfp).^2 + 1e-3);
x = real(ifftn(x));
% x = ATop(b, k_psfp);

% z 
z = x*1;
Uz = x - z;
% y
Dx = Der(x);
y = 1 * Dx;
Uy = Dx - y;

% these parameters might be important for speed and convergence, but now
% seem to work fine
alpha = 0.7;
rho = 1;

fvs = zeros(MAX_ITER, 1);
tolxz = zeros(MAX_ITER, 1);
data_err = zeros(MAX_ITER, 1);
tolX = zeros(MAX_ITER, 1);
tolX_inf = zeros(MAX_ITER, 1);
tolF = zeros(MAX_ITER, 1);

% here goes ADMM iterations
for it = 1 : MAX_ITER
    % X-update
    % solve min_x 0.5*|w.*(conv(x, psf) - b)|^2 + rho/2 * |x - b - z + Uz|_2^2 +
    % rho/2 * |Dx - y + Uy|_2^2
    alpha = max(alpha * 0.999, 1e-2);
    x_prev = x;
    if strcmp(X_upd_type, 'GD')
        dif =  (Aop(x, k_psfp) - b);
        g1 = ATop(w2 .* dif, k_psfp);
        g2 = rho * (x - z + Uz - base);
        g3 = rho * DTer(Dx - y + Uy);
        
        x = x - alpha * (g1 + g2 + g3);
        Dx = Der(x);
    elseif strcmp(X_upd_type, 'CG')
        error('not implemented');
        opts = [];
        opts.method = 'cg';
        opts.MaxIter = 10;
        opts.MaxFunEvals = 10;
        fobj = @(x)quad_fv(x, w, k_psfp, b, rho, z-u, size(b));
        x = minFunc(fobj, double(gather(x(:))), opts);
        x = reshape(x, size(b));
    end
    
    % Z-update
    % solve min_z  lambda * |z|_1 + rho/2 * |x - b - z + Uz|_2^2 
    z = proximal_l1(x + Uz - base, lambda / rho);
    Uz = Uz + x -base - z; % dual update
    
    % Y-update
    % solve min_y  lambda * |y|_{2,1} + rho/2 * |Dx - y + Uy|_2^2 
    tmp = Dx + Uy;
    tmp = reshape(tmp, [], 3)';
    y = proximal_euclidean(tmp, lambda_TV/rho);
    y = reshape(y', size(Dx));
    Uy = Uy + Dx - y; % dual update
    
    %BOOKKEEPING:
    tv_fv = sum(fl(sqrt( sum(Dx.^2, 4) )));
    data_err(it) = gather(sum(fl(abs(w.*dif).^2))/2);
    fv = data_err(it) + lambda * sum(abs(x(:) - base(:))) + lambda_TV * tv_fv;
    fvs(it) = gather(fv);
    tolxz(it) = gather(max(abs(x(:)-z(:)-base(:) )));
    tolX(it) = gather(norm(fl(x-x_prev))/norm(x(:)));
    tolX_inf(it) = gather(max(fl(abs(x-x_prev))) / max(abs(x(:))) );
    if it > 1
        tolF(it-1) = fvs(it) - fvs(it-1);
        if debug >= 2
            fprintf('it %d, fv=%e  tolX=%e, tolF=%e\n', it, fv, tolX(it), tolF(it-1));
        end
    end
    if it > 10 && tolX(it) < mtolX
        break;
    end
    if debug >= 3
        if mod(it, 10) == 0
            subplot(151);
            plot(fvs(1:it), '.-')
            xlabel('iter'); ylabel('Fcost'); grid on;
            subplot(152);
            plot(log(tolX(1:it)), '.-');
            xlabel('iter'); ylabel('log tolX'); grid on;
            subplot(153);
            plot(tolX_inf(1:it), '.-');
            xlabel('iter'); ylabel('tolXinf'); grid on;

            subplot(154);
            plot(tolF(1:it-1), '.-');
            xlabel('iter'); ylabel('tolF'); grid on;

            subplot(155);
            imagesc(x(:, :, 60)); colorbar;
            title('current recon');

            pause(0.005);
        end
    end
end
rec = x;

itinfo = [];
itinfo.fvs = fvs(1:it);
itinfo.data_err = data_err(1:it);
itinfo.tolX = tolX(1:it);
itinfo.tolF = tolF(1:it);
itinfo.tolX_inf = tolX_inf(1:it);

end

function [fv, gr] = quad_fv(x, w, k_psfp, b, rho, A, sz)
% helper for CG
    x = reshape(x, sz);
    x = cast(x, 'like', b);
    dif = w .* (Aop(x, k_psfp) - b);
    dif2 = x - A;
    fv = sum(dif(:).^2)/2 + rho/2 * sum(dif2(:).^2);
    gr = ATop(w .* dif, k_psfp) + rho * dif2;
    gr = gr(:);
    
    fv = double(gather(fv));
    gr = double(gather(gr));
end

function x = proximal_l1(a, lambda)
% minimizes for x
%   lambda * |x|_1 + 1/2 * (x - a)^2
    x =  sign(a) .* max(abs(a) - lambda, 0);
end

function x = proximal_euclidean(a, lambda)
% minimize for x:
% 1/2 sum_ij (x(i, j) - a(i, j))^2 + lambda sum_j sqrt(sum_i x(i, j))
% a is  m x n,  m is number of "channels", n is number of "pixels"
    nrms = sqrt(sum(a.^2, 1));
    idxs = nrms > 0;
    x = a * 0;
    tmp = max(1 - lambda ./ nrms(idxs), 0);
    atmp = a(:, idxs);
    x(:, idxs) = bsxfun(@times, atmp, tmp);
end    

function y = Aop(x, k_psfpad)
% forward conv
    kx = fftn(x);
    y = real(ifftn(kx .* k_psfpad));
end

function y = ATop(x, k_psfpad)
% transposed conv
    kx = fftn(x);
    y = real(ifftn( kx .* conj(k_psfpad) ));
end

function y = fl(x)
    y=x(:);
end

function y = Der(x)
% forward finite difference

%     y1 =  (circshift(x, 1, 1) - x)/2;
%     y2 =  (circshift(x, 1, 2) - x)/2;
%     y3 =  (circshift(x, 1, 3) - x)/2;
    % faster on gpu:
    y1 = -cat(1, x(1,:,:) - x(end, :, :), diff(x, 1, 1))/2;
    y2 = -cat(2, x(:,1,:) - x(:, end, :), diff(x, 1, 2))/2;
    y3 = -cat(3, x(:,:,1) - x(:,:, end), diff(x, 1, 3))/2;
%     norm(fl(y1-y11))
    
    y = cat(4, y1,y2,y3) / sqrt(3);
end

function y = DTer(x)
% transposed finite difference

%     y1 =  (circshift(x(:,:,:, 1), -1, 1) - x(:,:, :, 1))/2;
%     y2 =  (circshift(x(:,:,:, 2), -1, 2) - x(:,:, :, 2))/2;
%     y3 =  (circshift(x(:,:,:, 3), -1, 3) - x(:,:, :, 3))/2;
    % faster on gpu
    y1 =  cat(1, diff(x(:,:,:, 1), 1, 1), x(1,:,:, 1) - x(end, :, :, 1))/2;
    y2 =  cat(2, diff(x(:,:,:, 2), 1, 2), x(:,1,:, 2) - x(:, end, :, 2))/2;
    y3 =  cat(3, diff(x(:,:,:, 3), 1, 3), x(:,:,1, 3) - x(:, :, end, 3))/2;
    
    y = (y1 + y2 + y3) / sqrt(3);
end