imsz = [201, 201, 151];

img = zeros(imsz);
img(:,:, 130:135) = 1;

[n1,n2,n3] = ndgrid(1:imsz(1), 1:imsz(2), 1:imsz(3));
m1 = sqrt( (n1-100).^2 + (n2-100).^2+(n3-110).^2) < 20;
m2 = sqrt( (n1-125).^2 + (n2-125).^2+(n3-110).^2) < 15;
m3 = sqrt( (n1-140).^2 + (n2-140).^2+(n3-110).^2) < 7;
img = img + m1 + m2 + m3;

psz = 50;
[n1,n2,n3] = ndgrid(linspace(-1,1, psz));

psf = abs( sqrt(n1.^2 + n2.^2 + 2*n3.^2) - 0.9) < 3e-2;
psf = abs( sqrt(n1.^2 + n2.^2 + 2*n3.^2) - 0.4) < 3e-2;
psf = psf .* (n3 > 0.2);
psf = imgaussfilt3(psf, 1);
psf = psf / sum(psf(:));

img = imresize3(img, [101,101, 75]);
psf = imresize3(psf, [25,25,25]);

% do convolution
kimg = fftn(img);
psfp = padarray(psf, size(img) - size(psf), 'post');
% 
shftd = floor(size(psf)/2);
psfp = circshift(psfp, -shftd(1), 1);
psfp = circshift(psfp, -shftd(2), 2);
psfp = circshift(psfp, -shftd(3), 3);
kpsf = fftn(psfp, size(img));
dd = real(ifftn(kimg .* kpsf));

% add noise
noise_std = 0.04;
ddn = dd + randn(size(img)) * noise_std;
fprintf('SNR of %.3f\n', max(dd(:))/noise_std);
% add undersampling. Acceleration rate of 5
w = double(rand(size(ddn)) > 0.8);
ddn = ddn.*w;
% w can contain noise variance. The it shall be 1/std 

% this doesnt seem to help much
base_img = imgaussfilt3(img, 10);
%%
% you can use gpuArrays
dd = gpuArray(single(dd));
psf = gpuArray(single(psf));

% or cpu
% dd = double(gather(dd));
% psf = double(gather(psf));

tic
[rec, itinfo] = deconv_3d_sparse(ddn, psf, w, 0.01, 0.05, 0*base_img, 500, 1e-3, 3);
toc
rec = gather(rec);
%
sum(abs(rec(:)-img(:)))
%%
volumeViewer([rec, img])

%% here is the L-curve method for choosing lambda
lambda_TVs = exp(linspace(-7, 0, 10));
true_err = [];
data_err = [];

for i = 1 : numel(lambda_TVs)
    lambda = lambda_TVs(i);
    [rec, itinfo] = deconv_3d_sparse(ddn, psf, w, 0.00, lambda, 0*base_img, 500, 1e-4, 0);
    true_err(i) = gather(sum(abs(rec(:) - img(:)).^2 ));
    data_err(i) = gather(itinfo.data_err(end));
end
%%
hold off;
plot(lambda_TVs, log(true_err), 'r.-');
hold on;
plot(lambda_TVs, log(data_err), 'b.-');
grid on
xlabel('Lambda_T_V')
title('Heuristic method suggests taking the sharp bending of the L-curve (blue line)')