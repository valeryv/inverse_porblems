# inverse_porblems
Please see `example.m` for a self contained example.

SNR=3, acceleration rate 5:

![https://git.ee.ethz.ch/valeryv/inverse_porblems/blob/master/deconv_3d/example_recon.png](/deconv_3d/example_recon.png)


TODO:
* make sure that boundary conditions for convolution dont mess the real data
* GD might fail sometimes
* we can force nonnegativity 
* test different undersampling masks